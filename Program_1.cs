﻿using System;

namespace trabajos_itla
{
    class Program
    {
        static void Main(string[] args)
        {

            // Crear un programa que pida números positivos al usuario, y vaya calculando la suma de todos ellos (terminará cuando se teclea un número negativo o cero).

            int num, suma = 0; 
            Console.Clear();
            
            do
            {
                Console.WriteLine("En caso de finalizar introduce 0 o un numero negativo");
                Console.Write("Introduce cualquier numero positivo : "); 
                num = int.Parse(Console.ReadLine());

                if (num > 0 )
                {
                    suma  = suma + num;
                
                } else{
                    Console.Clear();
                    Console.WriteLine("------------Resultado Calculado------------");
                }
            
            } while (num > 0 );

            Console.Write("La suma de los numeros ingresados es: ");
            Console.WriteLine(suma);
        }
    }
}
