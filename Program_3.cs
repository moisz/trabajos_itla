﻿using System;

namespace trabajos_itla
{
    class Program
    {
        static void Main(string[] args)
        {

            //Crear un programa que muestre los primeros 10 números pares a partir del producto de (10 x 10).

            int contador = 0;
            int i = (10*10);
            
            do
            {
                i ++;
                if (i % 2 == 0)
                {
                    contador = contador + 1;
                    Console.WriteLine(i);
                }   
            } while (contador <10);
        }
    }
}
