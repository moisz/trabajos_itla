﻿using System;

namespace trabajos_itla
{
    class Program_2
    {
        static void Main(string[] args)
        {

            //Crea un programa que escriba en pantalla los números del 1 al 10, usando "do..while".

           int num = 1;
           do{
               
               Console.Write("Número: ");
               Console.WriteLine(num);
               num ++;

           }while(num <= 10);
           
           
        }
    }
}

